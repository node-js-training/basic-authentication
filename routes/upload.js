var express = require('express');
const jwt = require('jsonwebtoken');
const bodyParser = require('body-parser');
const { post } = require('../app');

var router = express.Router();
 
router.use(bodyParser({ extended: false  }));

const posts = [];

/* GET users listing. */
router.post('/', function(req, res, next) {
  // console.log(req.body);
  const file = req.file;
  console.log(file);
  const title = req.body.title;
  const description = req.body.description;
  posts.push({ title, description, file: file.filename });
  res.json(posts);
});


// delete request only admins has access to it.
module.exports = router;
