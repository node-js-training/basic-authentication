var express = require('express');
const jwt = require('jsonwebtoken');
const authRouter = require('./auth');
var router = express.Router();


const verifyUser = (req, res, next) => {
  const authHeader = req.headers.authorization;
  if (!authHeader) res.sendStatus(401);
  jwt.verify(authHeader.split(' ')[1], process.env.AUTH_SECRET, (err, payload) => {
    if (err) res.end('Invalid authentication token');
    next();
  });
}


const posts = [{title: 'access', content: 'access token practicing'}, { title: 'mysql', content: 'connecting to mysql server' }];

router.route('/')
  .get(verifyUser, function(req, res, next) {
    res.json(posts);
  })

// delete request only admins has access to it.
module.exports = router;
