var express = require('express');
var router = express.Router();
const Sequelize = require('sequelize');
var connection = require('../db');

var sequelize = new Sequelize(process.env.DB_DATABASE,
  process.env.DB_USER,
  process.env.DB_PASSWORD, {
    host: process.env.DB_HOST,
    dialect: 'mysql'
  });


  var Users = sequelize.define('users', {
    userId: Sequelize.STRING,
    password: Sequelize.STRING
  }, {
    freezeTableName: true // Model tableName will be the same as the model name
  });


/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.post('/register', (req, res, next) => {
  const userId = req.body.userId;
  const password = req.body.password;

  if (userId.length < 4 || userId.length >= 20) {
    res.statusCode = 400;
    res.json({ message: 'Userid is not valid' });
  }
  if (password.length < 10 || password.length >= 30) {
    res.statusCode = 400;
    res.json({ message: 'Invalid password. try with different password' });
  } 
  connection.query('CREATE TABLE IF NOT EXISTS users (userId varchar(20), password varchar(200))', (err, result, fields) => {
    if (err) res.end('Something went wrong while creating users table');

    //Verify already user is existing with same userId
    connection.query(`SELECT * FROM users WHERE userId='${userId}'`, (err, result, fields) => {
      if (err) res.end('Server error');
      if (result.length > 0) {
        res.end('There was some user already existing with provided details. try registering with different userId');
      } else {

        sequelize.sync().then(function() {
          return Users.create({
            userId,
            password
          });
        }).then(function(jane) {
          res.end(jane.get({
            plain: true
          }));
        });

        // connection.query(`INSERT INTO users values('${userId}', '${password}')`, (err, result) => {
        //   if (err) res.end('Error while inserting user');
        //   res.end('User registration is successfull');
        // })
      }
    })
  });

});

module.exports = router;
