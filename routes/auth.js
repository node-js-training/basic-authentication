const express = require('express');
const jwt = require('jsonwebtoken');
const connection = require('../db');
const refreshTokenDB = require('./refreshTokens');
const app = require('../app');

const authRouter = express.Router();
const SECRET_KEY = 'A SECRET KEY';

authRouter.post('/refresh', (req, res, next) => {
  const refreshToken = req.body.refreshToken;

  if (refreshTokenDB.includes(refreshToken)) {
    jwt.verify(refreshToken, process.env.REFRESH_SECRET, (err, payload) => {
      if (err) res.sendStatus(400);
      const user = payload;
      const authToken = jwt.sign(user, process.env.AUTH_SECRET, { expiresIn: '30s' });
      res.json({ authToken });
    });
  } else {
    res.end('Invalid refresh token');
  }
});

authRouter.post('/', (req, res, next) => {
  const userId = req.body.userId;
  const password = req.body.password;
  const query = `SELECT userId, password FROM users WHERE userId=? AND password=?`;
  connection.query( query, [userId, password], 
    (err, result, fields) => {
      if (err) res.end('There is server error. please try again');
      if (result.length === 0) {
        const err = new Error('User id and password is not valid. please try again');
        err.status = 406;
        next(err);
      } else {
        const user = result[0];
        const authToken =  jwt.sign({ ...user }, process.env.AUTH_SECRET, { expiresIn: '30s' });
        const refreshToken =  jwt.sign({ ...user }, process.env.REFRESH_SECRET);
        refreshTokenDB .push(refreshToken);
        res.json({ authToken, refreshToken });
      }
    });
})


//PREPARED QUERY
//ORM module

authRouter.post('/logout', (req, res) => {
  const refreshToken = req.body.refreshToken;
  refreshTokenDB.splice(refreshTokenDB.indexOf(refreshToken), 1);
  res.end('You are logged out');
})


module.exports = authRouter;