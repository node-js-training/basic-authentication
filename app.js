var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var multer = require('multer');
const cors = require('cors');
require('dotenv').config();


var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var postsRouter = require('./routes/posts');
var uploadRouter = require('./routes/upload');
const authRouter = require('./routes/auth');
const commentRouter = require('./routes/comments');
var app = express();

const refreshTokenDB = [];

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'images');
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + '-' + file.originalname)
  }
})

const fileFilter = (req, file, cb) => {
  cb(null, true);
  // if (file.mimetype === 'image/png') {
  //   cb(null, true);
  // } else{ 
  //   cb(new Error('Invalid file type'), false);
  // }
};

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());

app.use(multer({ storage: storage, fileFilter }).single('postImage'));

app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'images')));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);

app.use('/login', authRouter);

// app.use((req, res, next) => {
//   const authHeader = req.headers.authorization;
//   if (!authHeader) {
//     const err = new Error('No Authorization token is available');
//     err.status = 401;
//     next(err);
//   }
//   next();
// });

//users and posts are available to access for all authorized people.
app.use('/users', usersRouter);
app.use('/posts', postsRouter);
app.use('/comments', commentRouter);
app.use('/uploadPost', uploadRouter);
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.refreshTokenDB = refreshTokenDB;

module.exports = app;
